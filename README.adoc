= Collection of Brave search goggles

NOTE: This repository is mirrored to Gitlab from Codeberg since Brave search does not support Codeberg or any private hosting as source, if needing to interact with the project, please do it here: https://codeberg.org/jimbo/bravegoggle

Also while I'm happy to improve them, they are primarily written for my personal needs and biases.

Source url:: \https://gitlab.com/jim.bo/bravegoggle/-/raw/main/<filename>.goggle

Update url, as a note for developers:: https://search.brave.com/goggles/create

Brave goggles docs:: https://github.com/brave/goggles-quickstart/tree/main

Direct subscribe links:: https://search.brave.com/goggles?goggles_id=<urlencoded-source-url>

== Enabling by default on desktop firefox

. Navigate to about:config
. Find browser.urlbar.update2.engineAliasRefresh and toggle it to true
. Go to settings → search
. On the search engines there should be “Add” button
. The url should be: \https://search.brave.com/goggles?q=%s&goggles_id=<put-your-goggle-id-here> id is source url in urlencoded form
. Save and set your preferences as you normally would

== Urlencoded links - as note

`https%3A%2F%2Fgitlab.com%2Fjim.bo%2Fbravegoggle%2F-%2Fraw%2Fmain%2Fdeveloper.goggle``
